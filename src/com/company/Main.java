package com.company;

import com.espertech.esper.client.*;

import java.util.Random;

public class Main {

    public static void main(String[] args) throws InterruptedException {
	// write your code her
        EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider();
        String expression = "select avg(price) from com.company.OrderEvent.win:length(15000)";
        EPStatement statement = epService.getEPAdministrator().createEPL(expression);
        MyListener listener = new MyListener();
        statement.addListener(listener);
        Random rand = new Random();
        int n = 20000000;
        long startTime = System.currentTimeMillis();
        for(int i=0;i<n;++i){
            OrderEvent event = new OrderEvent("shirt", rand.nextDouble()*50);
            epService.getEPRuntime().sendEvent(event);
        }
        long endTime = System.currentTimeMillis();
        assert n==listener.counter;
        long diff = endTime - startTime;
        System.err.println("N= "+n+" "+"Time = "+ diff);
//        System.err.println(diff);
    }
}

