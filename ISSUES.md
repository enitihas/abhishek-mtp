ISSUES
======

1. One big issue with docker is that it requires root access. This means it is not feasible to deploy it in PC labs for lab usage by students at least.

2. There seem to be many experiences of people who are running hadoop and spark inside docker. However, most of them are not using kubernetes.

3. Integrating YARN and docker seems a painful task right now.
