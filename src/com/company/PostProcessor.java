package com.company;

import org.apache.storm.tuple.Tuple;

/**
 * Created by enitihas on 28/11/16.
 */
public interface PostProcessor {
    void postProcess(Tuple tuple, EsperStorm esperStorm);
}
