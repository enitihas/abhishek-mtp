package com.company;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import java.util.Map;
import java.util.Random;

/**
 * Created by enitihas on 17/11/16.
 */
class RandomSpout implements IRichSpout {

    private SpoutOutputCollector collector;

    @Override
    public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        collector = spoutOutputCollector;
    }

    @Override
    public void close() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }

    @Override
    public void nextTuple() {
        String name = "example";
        Random rand = new Random();
        Integer price = rand.nextInt(50);
        collector.emit(new Values(name,price));
    }

    @Override
    public void ack(Object o) {

    }

    @Override
    public void fail(Object o) {

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("name","price"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}

class RandomBolt extends BaseRichBolt {
    int n,counter=0;
    long startTime, endTime;
    public RandomBolt() {
    }

    public RandomBolt(int n) {
        this.n = n;
        startTime = System.currentTimeMillis();
    }

    private OutputCollector collector;
    EPServiceProvider epService;
    EPStatement statement;

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        collector = outputCollector;
        epService = EPServiceProviderManager.getDefaultProvider();
        String expression = "select avg(price) from com.company.OrderEvent.win:length(15000)";
        statement = epService.getEPAdministrator().createEPL(expression);
        MyListener listener = new MyListener();
        statement.addListener(listener);
    }

    @Override
    public void execute(Tuple tuple) {
//        counter++;
//        if(counter%1000000==0) {
//            endTime = System.currentTimeMillis();
//            long diff = endTime - startTime;
//            System.err.println("N= "+counter+" "+"Time = "+ diff);
//        }
        String name = tuple.getString(0);
        Integer price = tuple.getInteger(1);
        OrderEvent event = new OrderEvent(name,price);
        epService.getEPRuntime().sendEvent(event);
        collector.emit(new Values(name));
        collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("name"));
    }
}

public class EsperBolt {
    public static void main(String[] args) {
        int n = 20000000;
        Config config = new Config();
        config.setDebug(false);
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout(
                "s1", new RandomSpout()
        );

        builder.setBolt(
                "b1", new RandomBolt(n)
        ).shuffleGrouping("s1");

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("EsperStorm", config, builder.createTopology());
        Utils.sleep(1000000);
        cluster.killTopology("EsperStorm");

//        Thread.sleep(1000000);
        cluster.shutdown();
//        EsperStorm esperStrom = new EsperStorm();
//        BaseRichBolt bolt = esperStrom.new EsperStormBuilder()
//                .statement("Hello World")
//                .prepare(new BoltPreparer())
//                .preProcess(new PreProcessor())
//                .postProcess(new PostProcessor())
//                .execute(new BoltExecutor())
//                .build();

    }
}
