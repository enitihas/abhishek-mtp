package com.company;

import org.apache.storm.tuple.Tuple;

/**
 * Created by enitihas on 28/11/16.
 */
public interface BoltExecutor {
    void execute(Tuple tuple, EsperStorm esperStorm);
}
