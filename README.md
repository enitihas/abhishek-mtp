MTP Related Files and Resources
===============================

## Docker
1. Docker in Action (Book)
2. [Docker Docs](https://docs.docker.com)

## Kubernetes
1. [Are you ready to manage your infrastructure like Google.](http://www.jetstack.io/new-blog/2015/6/19/are-you-ready-to-manage-your-infrastructure-like-google-kubernetes-coming-to-a-cloud-near-you)
2. [What is Kubernetes.](http://kubernetes.io/)
3. [Kubernetes Past. (Google Research)](http://research.google.com/pubs/archive/44843.pdf)

Some more articles are at [Awesome Kubernetes](https://github.com/ramitsurana/awesome-kubernetes)
