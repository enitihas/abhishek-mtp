package com.company;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by enitihas on 28/11/16.
 */
public class EsperStorm {
    private OutputCollector collector;
    private EPServiceProvider epService;
    private EPStatement statement;
    private PreProcessor preProcessor;

    public EsperStorm() {
        map = new HashMap<>();
    }

    private PostProcessor postProcessor;
    Map<Object,Object> map;

    public OutputCollector getCollector() {
        return collector;
    }

    public EPServiceProvider getEpService() {
        return epService;
    }

    public EPStatement getStatement() {
        return statement;
    }

    public BoltPreparer getPreparer() {
        return preparer;
    }

    public BoltExecutor getBoltExecutor() {
        return boltExecutor;
    }

    public Declarer getDeclarer() {
        return declarer;
    }

    private BoltPreparer preparer;
    private BoltExecutor boltExecutor;
    /**
     *
     */
    public class EsperStormBuilder {
        private EsperStorm esperStorm = new EsperStorm();


        public EsperStormBuilder() {
            epService = EPServiceProviderManager.getDefaultProvider();
        }


        public EsperStormBuilder statement(String expression) {
            statement = epService.getEPAdministrator().createEPL(expression);
            return this;
        }

        public EsperStormBuilder prepare(BoltPreparer boltPreparer) {
            preparer = boltPreparer;
            return this;
        }

        public EsperStormBuilder preProcess(PreProcessor preProcessor1) {
            preProcessor = preProcessor1;
            return this;
        }

        public EsperStormBuilder postProcess(PostProcessor postProcessor1) {
            postProcessor = postProcessor1;
            return this;
        }

        public EsperStormBuilder execute(BoltExecutor boltExecutor1) {
            boltExecutor = boltExecutor1;
            return this;
        }

        public BaseRichBolt build() {
            BaseRichBolt bolt = new BaseRichBolt() {
                @Override
                public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
                    preparer.prepare(map, topologyContext, outputCollector);
                }

                @Override
                public void execute(Tuple tuple) {
                    preProcessor.preProcess(tuple,esperStorm);
                    boltExecutor.execute(tuple, esperStorm);
                    postProcessor.postProcess(tuple,esperStorm);
                }

                @Override
                public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
                    declarer.declare(outputFieldsDeclarer);
                }
            };
            return bolt;
        }
    }

    private Declarer declarer;

    public Object getOrDefault(Object key, Object defaultValue) {
        return map.getOrDefault(key, defaultValue);
    }

    public int size() {
        return map.size();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    public Object get(Object key) {
        return map.get(key);
    }

    public Object put(Object key, Object value) {
        return map.put(key, value);
    }

    public Object remove(Object key) {
        return map.remove(key);
    }
}
