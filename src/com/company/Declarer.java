package com.company;

import org.apache.storm.topology.OutputFieldsDeclarer;

/**
 * Created by enitihas on 28/11/16.
 */
public interface Declarer {
    void declare(OutputFieldsDeclarer outputFieldsDeclarer);
}
