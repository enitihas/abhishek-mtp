package com.company;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;

import java.util.Map;

/**
 * Created by enitihas on 28/11/16.
 */
public interface BoltPreparer {
    void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector);
}
