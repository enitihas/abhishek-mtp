package com.company;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;

public class MyListener implements UpdateListener {
    public int counter=0;
    private long endTime;

    public MyListener() {
        this.startTime = System.currentTimeMillis();
    }

    private long startTime;

    public void update(EventBean[] newEvents, EventBean[] oldEvents) {
        counter++;
        if(counter%1000000==0) {
            endTime = System.currentTimeMillis();
            long diff = endTime - startTime;
            System.err.println("N= "+counter+" "+"Time = "+ diff);
        }
//        EventBean event = newEvents[0];
//        System.err.println("avg=" + event.get("avg(price)"));
    }
}
