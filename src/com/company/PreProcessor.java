package com.company;

import org.apache.storm.tuple.Tuple;

/**
 * Created by enitihas on 28/11/16.
 */
public interface PreProcessor {
    void preProcess(Tuple tuple, EsperStorm esperStorm);
}
